import fs from 'fs'

export async function splitBySize (filePath: string, size: number | string, removeOriginalFile: boolean = false): Promise<string[]> {
  const partFiles = []

  const partSize = _parseSize(size)

  if (partSize === 0) {
    throw new Error('Really? Zero size?')     
  }
  const fileSize = await _getFileSize(filePath)
  const totalParts = Math.ceil(fileSize / partSize)
  const parts = []

  if (totalParts === 1) {
    throw new Error('Part size is bigger than the file size.')      
  }

  while (parts.length < totalParts) {
    parts.push({
      filePath: `${filePath}.${String(parts.length + 1).padStart(3, '0')}`,
      start: parts.length * partSize,
      end: Math.min((parts.length + 1) * partSize, fileSize) - 1
    })
  }
  for (let partInfo of parts) {
    partFiles.push(await _writePart(filePath, partInfo))
  }
  if (removeOriginalFile) {
    await _removeFile(filePath)
  }

  return partFiles
}

export async function splitByParts (filePath: string, parts: number, removeOriginalFile = false): Promise<string[]> {
  if (parts <= 1 || parts >= 1000) {
    throw new Error('Number of parts must be between 1 and 1000')
  }
  const fileSize = await _getFileSize(filePath)
  const partSize = Math.ceil(fileSize / parts)
  return splitBySize(filePath, partSize, removeOriginalFile)
}

export enum METHOD {
  PARTS = 0,
  SIZE = 1
}




function _parseSize (input: string | number): number {
  let string = String(input).toLowerCase().trim()
  let bytes = parseFloat(string)
  if (string.endsWith('kb')) {
    bytes *= 1024
  } else if (string.endsWith('mb')) {
    bytes *= 1024 * 1024
  } else if (string.endsWith('gb')) {
    bytes *= 1024 * 1024 * 1024
  }

  return Math.ceil(bytes)
}

function _writePart (filePath: string, partInfo: { filePath: string; start: number; end: number; }): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = fs.createReadStream(filePath, {
      encoding: undefined,
      start: partInfo.start,
      end: partInfo.end
    })
    const writer = fs.createWriteStream(partInfo.filePath)
    const pipe = reader.pipe(writer)

    pipe.on('error', (error) => reject(error))
    pipe.on('finish', () => resolve(partInfo.filePath))
  })
}

function _removeFile (filePath: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, (error) => {
      if (error) return reject(error)
      resolve()
    })
  })
}

function _getFileSize (filePath: string): Promise<number> {
  return new Promise((resolve, reject) => {
    fs.stat(filePath, (error, stats) => {
      if (error) return reject(error)
      if (!stats.isFile) return reject(new Error(`Error: "${filePath}" is not a valid file`))
      resolve(stats.size)
    })
  })
}
