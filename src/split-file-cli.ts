#!/usr/bin/env node
import prompts from 'prompts'
import { splitBySize, splitByParts, METHOD } from './split-file';

type PromptResults = 
  | {
    method: METHOD.SIZE
    size: string
    delete: boolean
  } 
  | {
    method: METHOD.PARTS
    parts: number
    delete: boolean
  }

const questions = [
  {
    type: 'select',
    name: 'method',
    message: 'Split method:',
    choices: [
      { title: 'Size', value: METHOD.SIZE },
      { title: 'Parts', value: METHOD.PARTS }
    ]
  },
  {
    type: (_: any, values: PromptResults) => values.method === METHOD.PARTS ? 'number' : null,
    name: 'parts',
    message: 'Number of parts:'
  },
  {
    type: (_: any, values: PromptResults) => values.method === METHOD.SIZE ? 'text' : null,
    name: 'size',
    message: 'Maximum size for each part: (e.g. 1024, 100kb, 3.5mb, 2.55gb)'
  },
  {
    type: 'toggle',
    name: 'delete',
    message: 'Delete original file?',
    initial: false,
    active: 'Yes',
    inactive: 'No'
  }
]

async function start () {
  const file = process.argv[2]
  let result: string[] | void = []

  if (!file) {
    console.error(`
  [Error] File not supplied.
    e.g. split-file my-big-file.7z
    `)
    return
  }

  const options = await prompts(questions) as PromptResults

  switch (options.method){
    case METHOD.PARTS:
      console.log(`
  [Info] Splitting "${file}"
         into ${options.parts || '"null"'} parts${options.delete ? ' and deleting original file.' : '.'}
      `)
      result = await splitByParts(file, options.parts, options.delete)
        .catch(e => console.error(`  [Error] ${e.message}
        `))
      break
    case METHOD.SIZE:
      console.log(`
  [Info] Splitting "${file}"
         into parts of ${options.size || '"null"'}${options.delete ? ' and deleting original file.' : '.'}
      `)
      result = await splitBySize(file, options.size, options.delete)
        .catch(e => console.error(`  [Error] ${e.message}
        `))
      break
  }

  if (result && result.length) {
    console.log(`  [Success] OK.
    `)
  } else {
    console.log(`  [Error] Cancelled.
    `)
  }
  
}

start()