***
# This package is deprecated. Please use __[axnglu](https://npmjs.org/axnglu)__ instead.
***

# split-file.js

> Splits files in `x` parts, or `x` parts of `y` size.
> Generates files in the form `original.ext.001 original.ext.002 original.ext.003 ...`

```
Input:
  my-file.7z     - 322MB

Output:
  my-file.7z.001 - 100MB
  my-file.7z.002 - 100MB
  my-file.7z.003 - 100MB
  my-file.7z.004 -  22MB

```

## Node.JS

#### Install

```
npm install split-file.js
```

#### API
```typescript
import { splitBySize, splitByParts } from 'split-file.js'

splitByParts (
  filePath: string,
  parts: number,
  deleteOriginalFile = false
) => Promise<string[]>

splitBySize (
  filePath: string,
  size: number | string,
  deleteOriginalFile = false
) => Promise<string[]>

```
> Both functions return a `Promise` that fulfills into an `Array` containing the paths of the generated files.
> Note: `size` can either be the number of `bytes` or use `kb`, `mb` or `gb` for units.

#### Examples

```javascript
// Import module
import { splitBySize, splitByParts } from 'split-file.js'

// Path of the file
const myFile = 'C:/some-dir/my-large-file.7z'


// Split file in parts of 100MB
const files = await splitBySize(myFile, 100 * 1024 * 1024)
// or
const files = await splitBySize(myFile, '100mb')


// Split in 3 parts
const files = await splitByParts(myFile, 3)


// Split the file in 3 parts and delete the original file
const files = await splitByParts(myFile, 3, true)

```


## CLI

#### Install

```
npm install -g split-file.js
```

#### Usage

```
$ split-file <file-path>
```

