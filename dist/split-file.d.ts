export declare function splitBySize(filePath: string, size: number | string, removeOriginalFile?: boolean): Promise<string[]>;
export declare function splitByParts(filePath: string, parts: number, removeOriginalFile?: boolean): Promise<string[]>;
export declare enum METHOD {
    PARTS = 0,
    SIZE = 1,
}
