#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prompts_1 = __importDefault(require("prompts"));
const split_file_1 = require("./split-file");
const questions = [
    {
        type: 'select',
        name: 'method',
        message: 'Split method:',
        choices: [
            { title: 'Size', value: split_file_1.METHOD.SIZE },
            { title: 'Parts', value: split_file_1.METHOD.PARTS }
        ]
    },
    {
        type: (_, values) => values.method === split_file_1.METHOD.PARTS ? 'number' : null,
        name: 'parts',
        message: 'Number of parts:'
    },
    {
        type: (_, values) => values.method === split_file_1.METHOD.SIZE ? 'text' : null,
        name: 'size',
        message: 'Maximum size for each part: (e.g. 1024, 100kb, 3.5mb, 2.55gb)'
    },
    {
        type: 'toggle',
        name: 'delete',
        message: 'Delete original file?',
        initial: false,
        active: 'Yes',
        inactive: 'No'
    }
];
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        const file = process.argv[2];
        let result = [];
        if (!file) {
            console.error(`
  [Error] File not supplied.
    e.g. split-file my-big-file.7z
    `);
            return;
        }
        const options = yield prompts_1.default(questions);
        switch (options.method) {
            case split_file_1.METHOD.PARTS:
                console.log(`
  [Info] Splitting "${file}"
         into ${options.parts || '"null"'} parts${options.delete ? ' and deleting original file.' : '.'}
      `);
                result = yield split_file_1.splitByParts(file, options.parts, options.delete)
                    .catch(e => console.error(`  [Error] ${e.message}
        `));
                break;
            case split_file_1.METHOD.SIZE:
                console.log(`
  [Info] Splitting "${file}"
         into parts of ${options.size || '"null"'}${options.delete ? ' and deleting original file.' : '.'}
      `);
                result = yield split_file_1.splitBySize(file, options.size, options.delete)
                    .catch(e => console.error(`  [Error] ${e.message}
        `));
                break;
        }
        if (result && result.length) {
            console.log(`  [Success] OK.
    `);
        }
        else {
            console.log(`  [Error] Cancelled.
    `);
        }
    });
}
start();
