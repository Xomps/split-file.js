"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
function splitBySize(filePath, size, removeOriginalFile = false) {
    return __awaiter(this, void 0, void 0, function* () {
        const partFiles = [];
        const partSize = _parseSize(size);
        if (partSize === 0) {
            throw new Error('Really? Zero size?');
        }
        const fileSize = yield _getFileSize(filePath);
        const totalParts = Math.ceil(fileSize / partSize);
        const parts = [];
        if (totalParts === 1) {
            throw new Error('Part size is bigger than the file size.');
        }
        while (parts.length < totalParts) {
            parts.push({
                filePath: `${filePath}.${String(parts.length + 1).padStart(3, '0')}`,
                start: parts.length * partSize,
                end: Math.min((parts.length + 1) * partSize, fileSize) - 1
            });
        }
        for (let partInfo of parts) {
            partFiles.push(yield _writePart(filePath, partInfo));
        }
        if (removeOriginalFile) {
            yield _removeFile(filePath);
        }
        return partFiles;
    });
}
exports.splitBySize = splitBySize;
function splitByParts(filePath, parts, removeOriginalFile = false) {
    return __awaiter(this, void 0, void 0, function* () {
        if (parts <= 1 || parts >= 1000) {
            throw new Error('Number of parts must be between 1 and 1000');
        }
        const fileSize = yield _getFileSize(filePath);
        const partSize = Math.ceil(fileSize / parts);
        return splitBySize(filePath, partSize, removeOriginalFile);
    });
}
exports.splitByParts = splitByParts;
var METHOD;
(function (METHOD) {
    METHOD[METHOD["PARTS"] = 0] = "PARTS";
    METHOD[METHOD["SIZE"] = 1] = "SIZE";
})(METHOD = exports.METHOD || (exports.METHOD = {}));
function _parseSize(input) {
    let string = String(input).toLowerCase().trim();
    let bytes = parseFloat(string);
    if (string.endsWith('kb')) {
        bytes *= 1024;
    }
    else if (string.endsWith('mb')) {
        bytes *= 1024 * 1024;
    }
    else if (string.endsWith('gb')) {
        bytes *= 1024 * 1024 * 1024;
    }
    return Math.ceil(bytes);
}
function _writePart(filePath, partInfo) {
    return new Promise((resolve, reject) => {
        const reader = fs_1.default.createReadStream(filePath, {
            encoding: undefined,
            start: partInfo.start,
            end: partInfo.end
        });
        const writer = fs_1.default.createWriteStream(partInfo.filePath);
        const pipe = reader.pipe(writer);
        pipe.on('error', (error) => reject(error));
        pipe.on('finish', () => resolve(partInfo.filePath));
    });
}
function _removeFile(filePath) {
    return new Promise((resolve, reject) => {
        fs_1.default.unlink(filePath, (error) => {
            if (error)
                return reject(error);
            resolve();
        });
    });
}
function _getFileSize(filePath) {
    return new Promise((resolve, reject) => {
        fs_1.default.stat(filePath, (error, stats) => {
            if (error)
                return reject(error);
            if (!stats.isFile)
                return reject(new Error(`Error: "${filePath}" is not a valid file`));
            resolve(stats.size);
        });
    });
}
